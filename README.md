### How to install

1. Clone the repository

2. In root directory copy `.env.example` in `.env` 

3. Install vendors:<br/><br/>
`composer install`<br/><br/>
PS: You need to install `composer`, just use:<br/><br/>
`apt install composer`

4. Change the database connection<br/><br/>
Edit `.env` file, and change these lines with your values<br/><br/>
`DATABASE_URL=mysql://db_user:db_pwd@db_host:db_port/db_name`

5. Initialize database:<br/><br/>
`php bin/console doctrine:database:create`<br/>
`php bin/console doctrine:migrations:migrate`

6. Screenshots<br/><br/>
a. Add an account<br/>
![](img/account1.png)<br/>
b. Get the access token<br/>
![](img/account2.png)<br/>
c. Manage the profile<br/>
![](img/account.png)<br/><br/>
d. See your statuses<br/>
![](img/compose1.png)<br/>
e. Compose new ones<br/>
![](img/compose2.png)<br/>