<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 14/04/19
 * Time: 19:16
 */

namespace App\Controller;


use App\Repository\MastodonAccountRepository;
use App\Services\Mastodon_api;
use App\SocialEntity\MastodonAccount;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    /**
     * @Route("/", name="default")
     */
    public function index()
    {
        return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
    }

    /**
     * @Route("/login", name="default")
     */
    public function login()
    {
        return $this->redirect($this->generateUrl('sonata_user_admin_security_login'));
    }

    /**
     * @Route("/fedandco_mention", name="fedandco_mention")
     */
    public function mention(Request $request, Mastodon_api $mastodonAPI, MastodonAccountRepository $mastodonAccountRepository)
    {
        $search = $request->query->get("q");
        $type = $request->query->get("type");
        $full = $request->query->get("full");
        $acct = explode("@", $full)[0];
        $instance = explode("@", $full)[1];
        $account = $mastodonAccountRepository->findOneBy(['acct' => $acct, 'instance' => $instance]);
        $mastodonAPI->set_token($account->getToken(), "Bearer");
        $mastodonAPI->set_url("https://".$account->getInstance());
        $val = $mastodonAPI->search(['q' => trim($search), 'type'=>$type]);
        $response = new Response(json_encode($val['response']));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
}