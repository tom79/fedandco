<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 06/03/19
 * Time: 11:43
 */

namespace App\Controller;


use App\Entity\MastodonAccount;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class NotificationCRUDController extends CRUDController {



    public function showAction($id = null)
    {
        if( empty($id))
            throw new NotFoundHttpException();


        //$id is the id of a project
        $socialAccount = $this->getDoctrine()->getRepository("App:MastodonAccount")->findOneBy(['id' => $id]);
        if( !$socialAccount instanceof MastodonAccount)
            throw new NotFoundHttpException();
        if(!$this->get('security.authorization_checker')->isGranted('view', $socialAccount))
            throw new AccessDeniedHttpException();


        $mastodonApi = $this->get('mastodon.api');

        $mastodonApi->set_url("https://" . $socialAccount->getInstance());
        $mastodonApi->set_token($socialAccount->getToken(), "Bearer");

        $params = [];
        $_n = $mastodonApi->notifications($params);
        $notifications = $mastodonApi->getNotifications($_n['response']);

        //Here the social has been found and the authenticated user is a contributor of the project
        return $this->render('Mastodon/Notifications/show.html.twig',['account' => $socialAccount, 'notifications' => $notifications]);
    }



    public function loadMoreAction($id = null){

        $max_id =  $this->getRequest()->get('max_id');
        $type = $this->getRequest()->get('type');
        if( !$type)
            $type = "all";

        if( empty($id))
            throw new NotFoundHttpException();


        //$id is the id of a project
        $socialAccount = $this->getDoctrine()->getRepository("App:MastodonAccount")->findOneBy(['id' => $id]);
        if( !$socialAccount instanceof MastodonAccount)
            throw new NotFoundHttpException();
        if(!$this->get('security.authorization_checker')->isGranted('view', $socialAccount))
            throw new AccessDeniedHttpException();

        $mastodonApi = $this->get('mastodon.api');

        $mastodonApi->set_url("https://" . $socialAccount->getInstance());
        $mastodonApi->set_token($socialAccount->getToken(), "Bearer");

        $params = [];
        if( $type != "all"){
            switch ($type){
                case "follow":
                    $params['exclude_types[]'][] = "favourite";
                    $params['exclude_types[]'][] = "reblog";
                    $params['exclude_types[]'][] = "mention";
                    break;
                case "boost":
                    $params['exclude_types[]'][] = "favourite";
                    $params['exclude_types[]'][] = "follow";
                    $params['exclude_types[]'][] = "mention";
                    break;
                case "favourite":
                    $params['exclude_types[]'][] = "follow";
                    $params['exclude_types[]'][] = "reblog";
                    $params['exclude_types[]'][] = "mention";
                    break;
                case "mention":
                    $params['exclude_types[]'][] = "favourite";
                    $params['exclude_types[]'][] = "follow";
                    $params['exclude_types[]'][] = "reblog";
                    break;
            }
        }
        if( $max_id != null){
            $params['max_id'] = $max_id;
        }
        $_n = $mastodonApi->notifications($params);
        $notifications = $mastodonApi->getNotifications($_n['response']);
        $data['max_id'] = $_n['max_id'];
        $data['min_id'] = $_n['min_id'];
        $data['html'] = $this->renderView('Mastodon/Notifications/Ajax/layout.html.twig', ['notifications' => $notifications]);
        return new JsonResponse($data);
    }
}