<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 05/03/19
 * Time: 13:38
 */

namespace App\Admin;
use App\Entity\User;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormTypeInterface;
use Sonata\UserBundle\Admin\Model\UserAdmin as SonataUserAdmin;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserAdmin extends SonataUserAdmin
{


    /**
     * {@inheritdoc}
     */
    public function configureFormFields(FormMapper $formMapper): void
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        if( $this->isCurrentRoute('edit') &&  !$this->getConfigurationPool()->getContainer()->get('security.authorization_checker')->isGranted('edit', $this->getSubject()))
            throw new AccessDeniedHttpException();
        $translator = $this->getTranslator();
        // define group zoning
        $formMapper
            ->tab($translator->trans('project.role.user',[], 'fedandco', 'en'))
            ->with('Profile', ['class' => 'col-md-6'])->end()
            ->with('General', ['class' => 'col-md-6'])->end()
            ->with('Social', ['class' => 'col-md-6'])->end()
            ->end()
            /*->tab('Security')
            ->with('Status', ['class' => 'col-md-4'])->end()
            ->with('Groups', ['class' => 'col-md-4'])->end()
            ->with('Keys', ['class' => 'col-md-4'])->end()
            ->with('Roles', ['class' => 'col-md-12'])->end()
            ->end()*/
        ;

        try {
            $now = new \DateTime();
        } catch (\Exception $e) {
        }

        $genderOptions = [
            'choices' => \call_user_func([$this->getUserManager()->getClass(), 'getGenderList']),
            'required' => true,
            'translation_domain' => $this->getTranslationDomain(),
        ];

        // NEXT_MAJOR: Remove this when dropping support for SF 2.8
        if (method_exists(FormTypeInterface::class, 'setDefaultOptions')) {
            $genderOptions['choices_as_values'] = true;
        }
        ;
        if (($image = $this->getSubject()) && ($webPath = $image->getProfilePicture())) {
            $container = $this->getConfigurationPool()->getContainer();
            $provider = $container->get($image->getProfilePicture()->getProviderName());
            $format = $provider->getFormatName($image->getProfilePicture(), "small");
            $url = $provider->generatePublicUrl($image->getProfilePicture(), $format);
            $fileFieldOptions['help'] = '<img src="'.$url.'" class="admin-preview"/>';
        }else{
            $fileFieldOptions['help'] = '<img src="/bundles/sonatauser/default_avatar.png" class="admin-preview"/>';
        }

        $formMapper
            ->tab($translator->trans('project.role.user',[], 'fedandco', 'en'))
            ->with('General')
            ->add('username')
            ->add('email')
            ->add('plainPassword', TextType::class, [
                'required' => (!$this->getSubject() || null === $this->getSubject()->getId()),
            ])
            ->end()
            ->with('Profile')
            /*->add('dateOfBirth', DatePickerType::class, [
                'years' => range(1900, $now->format('Y')),
                'dp_min_date' => '1-1-1900',
                'dp_max_date' => $now->format('c'),
                'required' => false,
            ])*/
            ->add('profile_picture', MediaType::class,[
                'provider' => 'sonata.media.provider.image',
                'label' => $translator->trans('user.avatar',[], 'fedandco', 'en'),
                'data_class'   =>  'App\Entity\Media',
                'context'  => 'profile_picture'
            ], $fileFieldOptions)
            ->add('firstname', null, ['required' => false])
            ->add('lastname', null, ['required' => false])
            ->add('website', UrlType::class, ['required' => false])
            ->add('biography', TextareaType::class, ['required' => false])

            ->end()
            ->with('Social')
            ->add('facebook', TextType::class, ['required' => false, 'label' => 'Facebook'])
            ->add('twitter', TextType::class, ['required' => false, 'label' => 'Twitter'])
            ->add('mastodon', TextType::class, ['required' => false, 'label' => 'Mastodon'])
            ->end()
            ->end()
            //->tab('Security')
            /*->with('Status')
            ->add('enabled', null, ['required' => false])
            ->end()
            ->with('Groups')
            ->add('groups', ModelType::class, [
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->end()
            ->with('Roles')
            ->add('realRoles', SecurityRolesType::class, [
                'label' => 'form.label_roles',
                'expanded' => true,
                'multiple' => true,
                'required' => false,
            ])
            ->end()*/

            /*->with('Keys')
            ->add('token', null, ['required' => false])
            ->add('twoStepVerificationCode', null, ['required' => false])
            ->end()
            ->end()*/
        ;
    }


    public function createQuery($context = 'list')
    {
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->in($query->getRootAliases()[0] . '.id', ':contacts')
        );
        $query->setParameter('contacts', $user);

        return $query;
    }



    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {

        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $id = $this->getSubject()->getId();
        $owner = false;
        if( $user->getId() == $id){
            $owner = true;
        }
        $translator = $this->getTranslator();
        $showMapper
            ->with('General')
            ->add('profile_picture', null, [
                'label' => $translator->trans('user.avatar',[], 'fedandco', 'en'),
                'template' => 'Sonata/UserAdmin/profile_picture.html.twig'
            ])
            ->add('username');
        if($owner)
            $showMapper->add('email');
        $showMapper->end()
            ->with('Profile')
            ->add('firstname')
            ->add('lastname')
            ->add('website')
            ->add('biography')
            ->end()
            ->with('Social')
            ->add('facebook',  null, ['label' => 'Facebook'])
            ->add('twitter',  null,['label' => 'Twitter'])
            ->add('mastodon', null, ['label' => 'Mastodon'])
            ->end();
       /* if($owner)
            $showMapper->with('Security')
            ->add('token')
            ->add('twoStepVerificationCode')
            ->end()
        ;*/
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Only `list` and `edit` route will be active
        $collection->clearExcept(['list', 'edit','show']);

    }

    public function toString($object)
    {
        return $object instanceof User
            ? $object->getUsername()
            : 'User ';
    }


}