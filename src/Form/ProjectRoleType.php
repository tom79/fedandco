<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 13/03/19
 * Time: 14:57
 */

namespace App\Form;
use App\Entity\Project;
use App\Entity\ProjectRole;
use App\Entity\User;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface as Translator;

class ProjectRoleType extends AbstractType {


    private $securityContext;
    private $translator;

    public function __construct(Security $securityContext, Translator $translator)
    {
        $this->securityContext = $securityContext;
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $project = $options['project'];
        $builder->add('project', EntityType::class,[
            'class' => Project::class,
            'attr' => ['class' => 'hide'],
            'label' => false,
            'query_builder' => function($er)  use ($project) {
                $query =  $er->createQueryBuilder('p')
                    ->andWhere(
                        'p.id= :project'
                    )
                    ->setParameter('project',   $project);

                return $query;
            }
        ]);
        $builder->add('user', EntityType::class, array(
            'class' => User::class,
            'attr' => ['class' => 'inline-choice'],
            'query_builder' => function($er) {
                $query =  $er->createQueryBuilder('u')
                    ->leftJoin('App:Invitation', 'i1', Expr\Join::WITH, 'u.id = i1.toUser')
                    ->leftJoin('App:Invitation', 'i2', Expr\Join::WITH, 'u.id = i2.fromUser')
                    ->andWhere(
                        '(i1.fromUser = :user OR i1.toUser = :user OR i2.fromUser = :user OR i2.toUser = :user ) AND (i1.accepted = 1 OR i2.accepted = 1) AND u.id != :user'
                    )
                    ->setParameter('user',   $this->securityContext->getToken()->getUser());

                return $query;
            }));
        $builder->add('role',ChoiceFieldMaskType::class,
            [
                'attr' => ['class' => 'inline-choice'],
                'choices' =>
                [
                    $this->translator->trans('project.role.user',[], 'fedandco', 'en') => 'USER',
                    $this->translator->trans('project.role.author',[], 'fedandco', 'en') => 'AUTHOR',
                    $this->translator->trans('project.role.admin',[], 'fedandco', 'en') => 'ADMIN',
                ]
            ]);
    }




    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectRole::class,
        ]);
        $resolver->setRequired(array(
            'project'
        ));
    }

}