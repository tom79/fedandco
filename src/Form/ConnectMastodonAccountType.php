<?php
/**
 * Created by fedandco.
 * User: tom79
 * Date: 13/03/19
 * Time: 14:57
 */

namespace App\Form;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;

class ConnectMastodonAccountType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        switch ($options['flow_step']) {
            case 1:
                $builder->add('host', null, [
                    'label' => 'Your instance',
                ]);
                break;
            case 2:
                // This form type is not defined in the example.
                $builder->add('code', null, [
                    'label' => 'Your authorization code',
                ]);
                $builder->add('client_id', HiddenType::class);
                $builder->add('client_secret', HiddenType::class);
                break;
        }
    }

    public function getBlockPrefix() {
        return 'addMastodonAccount';
    }
}