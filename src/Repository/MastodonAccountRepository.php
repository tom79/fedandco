<?php

namespace App\Repository;

use App\Entity\MastodonAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MastodonAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method MastodonAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method MastodonAccount[]    findAll()
 * @method MastodonAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MastodonAccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MastodonAccount::class);
    }



    // /**
    //  * @return MastodonAccount[] Returns an array of MastodonAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MastodonAccount
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
